﻿/*
Napisać program, który wczyta ze standardowego wejścia nazwiska i imiona studentów w grupie (pierwsza linijka ma zawierać liczebność grupy), 
posortuje je alfabetycznie (wykorzystując algorytm sortowania bąbelkowego) i wyświetli na standardowe wyjście, dodając obok nazwiska nazwę pluszowego zwierzątka, które wygrywa dana osoba. 
Nagrodą jest zwierzątko, któremu odpowiada spełnienie pierwszego z poniższych warunków:
    Eskulapa - nazwisko zawiera co najmniej 2 litery e
    Kokoszka - nazwisko zaczyna się od sylaby ko
    Orzeł - nazwisko zawiera literę o
    Lew - nazwisko kończy się na ski/ska
    Króliczek - nagroda pocieszenia

Etapy rozwiązania (po 1 pkt.):
    Wczytanie liczebności grupy oraz utworzenie dwóch tablic wskaźników char*.
    Wczytywanie w pętli nazwisk i imion do buforów, tworzenie dynamicznych tablic dostosowanych do przechowania tychże oraz skopiowanie tam nazwiska i imiona oraz "podczepienie" pod tablice z pkt. 1. (2 pkt.)
    Wysortowanie daych bąbelkowo (2 pkt.).
    Sprawdzenie warunków 1-2.
    Sprawdzenie warunków 3-4.
    Instrukcje if sprawdzające powyższe tudzież przyznające nagrodę pocieszenia.
    Wyświetlenie pełnych rekordów na standardowe wyjście.
*/

#include <stdio.h>
#include <string.h>

// Struktura osoby
typedef struct {
    char* imię;
    char* nazwisko;
    char* nagroda;
} Osoba;

// Zmienne globalne
Osoba* bazaDanych;
int iloscRekordow;
FILE* plik;

// Funkcja otwierająca zadany plik
void getFile() {
    char filename[40];
    printf("Podaj nazwe pliku z danymi do posortowania:\n");
    scanf("%s", &filename);
    plik = fopen(filename, "r");
}

// Pobranie danych osób z pliku do bazy
void loadRecords() {
    bazaDanych = (Osoba*)malloc(iloscRekordow * sizeof(Osoba));                         // Rezerwacja pamięci na bazę danych osób
    char bufory[2][40];                                                                 // Bufory do wczytywania danych
    int i;

    // Wczytanie imion i nazwisk do tablicy Osób
    for (i = 0; i < iloscRekordow; i++) {
        // Wczytanie imion i nazwisko do buforów
        fscanf(plik, "%s %s", &bufory[1], &bufory[0]);

        // Alokacja pamięci dla imienia i nazwiska
        bazaDanych[i].imię = malloc(strlen(bufory[0]) + 1);
        bazaDanych[i].nazwisko = malloc(strlen(bufory[1]) + 1);

        // Skopiowanie danych do struktury
        strcpy(bazaDanych[i].imię, bufory[0]);
        strcpy(bazaDanych[i].nazwisko, bufory[1]);
    }
}

// Zamiana elementów tablicy
void swapInDatabase(int a, int b) {
    Osoba tmp = bazaDanych[a];
    bazaDanych[a] = bazaDanych[b];
    bazaDanych[b] = tmp;
}

// Zwraca imię i nazwisko danej osoby do porównania
char* fullName(Osoba os) {
    char* fullName = malloc(strlen(os.nazwisko) + strlen(os.imię) + 1);
    strcpy(fullName, os.nazwisko);
    strcat(fullName, " ");
    strcat(fullName, os.imię);
    return fullName;
}

// Sortowanie bąbelkowe
void bubbleSort() {
    int i, j, changed;

    for (i = 0; i < iloscRekordow - 1; ++i) {
        changed = 0;                                                                    // Resetuje wskaźnik dokonania zamiany
        for (j = 0; j < iloscRekordow - 1 - i; j++)
            if (strcmp(fullName(bazaDanych[j]), fullName(bazaDanych[j + 1])) > 0) {     // Porównuje pełne imię i nazwisko obecnej i następnej osoby
                swapInDatabase(j, j + 1);                                               // Jeśli obecny 'jest większy' od następnego następuje ich zamiana
                changed = 1;                                                            // Wskaźnik dokonania zmiany ustawiamy odpowiednio
            }
        if (!changed) break;                                                            // Jeśli żadna zmiana nie nastąpiła, to zakończ sortowanie wcześniej
    }
}

// Przydziel nagrody do danych osób
void giveRewards() {
    int i;
    char* o = "o", e = "e", ko = "ko", ski = "ski", ska = "ska";

    for (i = 0; i < iloscRekordow; i++) {
        bazaDanych[i].nagroda = malloc(10);

        bazaDanych[i].nagroda = "Kroliczek";                                            // Domyślna nagroda pocieszenia

        // Eskulapa dla nazwisk zawierających conajmniej 2 litery 'e'
        //if () bazaDanych[i].nagroda = "Eskulapa";
        // Kokoszka gdy naziwsko zaczyna się od sylaby 'ko'        
        //else if () bazaDanych[i].nagroda = "Kokoszka";
        // Orzeł jeśli nazwisko zawiera literę 'o'
        //else if () bazaDanych[i].nagroda = "Orzel";
        // Lew gdy nazwisko kończy się na 'ski'/'ska'
        //else if () bazaDanych[i].nagroda = "Lew";
    }
}

int main() {
    int i;

    //getFile();                                                                          // Otworzenie zadanego przez użytkownika pliku
    plik = fopen("osoby.txt", "r");                                                     // Otworzenie pliku 'osoby.txt'    
    fscanf(plik, "%d", &iloscRekordow);                                                 // Pobranie ilości rekordów z pierwszej linijki pliku
    loadRecords();                                                                      // Pobranie danych osób z pliku do bazy
    bubbleSort();                                                                       // Sortowanie bąbelkowe
    giveRewards();                                                                      // Przydziel nagrody do danych osób

    // Wypisanie posortowanych nazwisk i imion wraz z nagrodami
    for (i = 0; i < iloscRekordow; i++)
        printf("%s %s \t : %s\n", bazaDanych[i].nazwisko, bazaDanych[i].imię, bazaDanych[i].nagroda);

    getch();
    return 0;
}